cordova.define("cordova-plugin-x9-barcode-scanner.x9barcodescanner", function(require, exports, module) { var exec = require('cordova/exec');

exports.getAPIVersion = function (success, error) {
    exec(success, error, 'x9barcodescanner', 'getAPIVersion', [""]);
};

exports.getBarcodeReaderType = function (success, error) {
    exec(success, error, 'x9barcodescanner', 'getBarcodeReaderType', [""]);
};

exports.getCharset = function (success, error) {
    exec(success, error, 'x9barcodescanner', 'getCharset', [""]);
};

exports.activateScanner = function (arg0, success, error) {
    exec(success, error, 'x9barcodescanner', 'activateScanner', [arg0]);
};

exports.resetToFactorySettings = function (success, error) {
    exec(success, error, 'x9barcodescanner', 'resetToFactorySettings', [""]);
};


});
